# Dinckelman's bspwm dotfiles

### Important

The dotfiles for Polybar are a borrowed and somewhat modified version of the Polybar-1 config from [adi1090x]("https://github.com/adi1090x/polybar-themes"). 
All credits for the theme, scripts, and modules go to the original developer.

### Required Packages

- `xorg-xrandr` Xorg scripts for handling displays, primarily scaling
- `bspwm` Window manager 
- `sxhkd` Hotkey manager
- `polybar` Menubar
- `termite` Terminal emulator
- `rofi` Application launcher
- `feh` Wallpaper manager
- `nerd-fonts-complete` Collection of a ton of different font and icon presets

### Recommended Packages

- `xorg-xinit` Xorg startup manager
- `xfce4-notifyd` Notification manager with minimal config
- `acpilight` Backlight manager, allows the use of function keys
- `playerctl` Handles track switching when keybinds are used
- `wmname` Patches for Java application rendering under bspwm

### Optional Packages

- `polybar-spotify-git` Spotify d-bus plugin for Polybar
- `spicetify-cli` Spotify theme engine
- `spicetify-themes-git` Spicetify theme compilation
- `flameshot` Screenshot manager for X, not unlike ShareX

### Instructions

Before starting bspwm at all, get familiar with keybinds located in `sxhkdrc` and update terminal info in `bspwmrc`, in case your default is not `termite`

```bash
mkdir -p ~/.config/{bspwm,sxhkd,polybar,termite,rofi,feh}
rsync -a --exclude README.md --exclude assets * ~/.config/
chmod +x ~/.config/bspwm/bspwmrc
rsync -a assets/wallpaper.jpg ~/.config/feh/
```

### Optional Configs

```bash
chmod -R 777 /opt/spotify # Obtain Permissions
spicetify backup apply enable-devtool # Enable modifications for Spotify
spicetify config current_theme YoutubeDark # Apply and update theme
spicetify apply
```

### Screenshots and Misc

![](assets/1.png)
![](assets/2.jpg)
![](assets/wallpaper.jpg)